﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OrangeTreeSim
{
    public class OrangeTree
    {
        private int age;
        private int height;
        private bool treeAlive;
        private int numOranges;
        private int orangesEaten;

        public int Age
        {
            set
            {
                if (value >= 0)
                {
                    this.age = value;
                }
                else
                {
                    Console.WriteLine("Invalid age!");
                }
            }
            get { return age; }
        }

        public int Height
        {
            get { return height; }
            set { height = value; }
        }

        public bool TreeAlive
        {
            get { return treeAlive; }
            set { treeAlive = value; }
        }

        public int NumOranges
        {
            get { return numOranges; }
        }

        public int OrangesEaten
        {
            get { return orangesEaten; }
        }

        public void OneYearPasses()
        {
            treeAlive = true;
            age++;
            if (age < 80)
            {
                numOranges = (age * 5) - 5;
                height += 2;
            }
            else if (age >= 80)
            {
                treeAlive = false;
                numOranges = 0;
                height += 0;
            }
            orangesEaten = 0;
        }

        public void EatOrange(int count)
        {
            int canEat = Math.Min(count, numOranges);
            numOranges -= canEat;
            orangesEaten += canEat;

        }

    }
}
